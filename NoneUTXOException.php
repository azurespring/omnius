<?php
/**
 * NoneUTXOException.php
 *
 * @author tsai phan <phanalpha@hotmail.com>
 */
namespace AzureSpring\Omnius;

/**
 * The token sender is determined based on the first transaction input,
 * while other arbitrary inputs can be added to a transaction.
 */
class NoneUTXOException extends \RuntimeException
{
}
