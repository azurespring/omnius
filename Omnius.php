<?php
/**
 * Omnius.php
 *
 * @author tsai phan <phanalpha@hotmail.com>
 */
namespace AzureSpring\Omnius;

use AzureSpring\Redbit\Parameter\FundRawTransactionOptions;
use AzureSpring\Redbit\Redbit;
use AzureSpring\Redbit\Result\FundRawTransactionResult;
use Psr\Log\LoggerInterface;

/**
 * OmniCore minus.
 */
class Omnius extends Redbit implements OmniusInterface
{
    protected $minconf;

    protected $blocks;

    /**
     * Constructor.
     *
     * @param \GuzzleHttp\Client $client
     * @param int                $minconf
     * @param int                $blocks  The maximum number of blocks a transaction should have to wait before
     *                                    it is predicted to be included in a block. Has to be between 2 and 25 blocks
     * @param LoggerInterface    $logger
     */
    public function __construct(\GuzzleHttp\Client $client, int $minconf = 1, int $blocks = 3, LoggerInterface $logger)
    {
        parent::__construct($client, true, $logger);

        $this->minconf = $minconf;
        $this->blocks = $blocks;
    }

    /**
     * {@inheritDoc}
     */
    public function omniListTransactions(string $address = '*', int $count = 10, int $skip = 0, int $startBlock = 0, int $endBlock = 999999): array
    {
        return $this->remoteCall(
            'omni_listtransactions',
            [
                'address' => $address,
                'count' => $count,
                'skip' => $skip,
                'startBlock' => $startBlock,
                'endBlock' => $endBlock,
            ],
            Result\Transaction::class.'[]'
        );
    }

    /**
     * {@inheritDoc}
     */
    public function omniGetTransaction(string $txid): Result\Transaction
    {
        return $this->remoteCall('omni_gettransaction', ['txid' => $txid], Result\Transaction::class);
    }

    /**
     * {@inheritDoc}
     */
    public function omniGetBalance(string $address, int $propertyId): Result\Balance
    {
        return $this->remoteCall(
            'omni_getbalance',
            [
                'address' => $address,
                'propertyid' => $propertyId,
            ],
            Result\Balance::class
        );
    }

    /**
     * {@inheritDoc}
     */
    public function omniCreateSimpleSend(int $propertyId, string $amount): string
    {
        return $this->remoteCall('omni_createpayload_simplesend', ['propertyid' => $propertyId, 'amount' => $amount]);
    }

    /**
     * {@inheritDoc}
     */
    public function omniCreateRawTransaction(?string $rawTx, string $payload): string
    {
        return $this->remoteCall('omni_createrawtx_opreturn', ['rawtx' => $rawTx, 'payload' => $payload]);
    }

    /**
     * {@inheritDoc}
     */
    public function omniCreateRawTransactionReference(?string $rawTx, string $refAddress, ?string $amount = null): string
    {
        $params = ['rawtx' => $rawTx, 'destination' => $refAddress];
        if ($amount) {
            $params['amount'] = $amount;
        }

        return $this->remoteCall('omni_createrawtx_reference', $params);
    }

    /**
     * {@inheritDoc}
     */
    public function omniCreateRawSimpleSendTransaction(string $fromAddress, string $toAddress, int $propertyId, string $amount, ?string $changeAddress = null): FundRawTransactionResult
    {
        $output = current($this->listUnspent($this->minconf, 9999999, [$fromAddress]));
        if (!$output) {
            throw new NoneUTXOException();
        }

        $payload = $this->omniCreateSimpleSend($propertyId, $amount);
        $rawTx = $this->createRawTransaction([$output->asInput()], []);
        $rawTx = $this->omniCreateRawTransaction($rawTx, $payload);
        $rawTx = $this->omniCreateRawTransactionReference($rawTx, $toAddress);

        return $this->fundRawTransaction(
            $rawTx,
            (new FundRawTransactionOptions())
                ->setChangeAddress($changeAddress ?? $fromAddress)
                ->setChangePosition(0)
                ->setFeeRate($this->estimateFee($this->blocks))
        );
    }
}
