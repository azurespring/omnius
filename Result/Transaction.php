<?php
/**
 * Transaction.php
 *
 * @author tsai phan <phanalpha@hotmail.com>
 */
namespace AzureSpring\Omnius\Result;

/**
 * Information about an Omni transaction.
 */
class Transaction
{
    /* Transaction types */
    const SIMPLE_SEND = 0;
    const SEND_TO_OWNERS = 3;

    /**
     * The hex-encoded hash of the transaction
     *
     * @var string
     */
    private $txid;

    /**
     * The Bitcoin address of the sender
     *
     * @var string
     */
    private $sendingaddress;

    /**
     * A Bitcoin address used as reference (if any)
     *
     * @var string
     */
    private $referenceaddress;

    /**
     * Whether the transaction involves an address in the wallet
     *
     * @var bool
     */
    private $mine;

    /**
     * The number of transaction confirmations
     *
     * @var int
     */
    private $confirmations;

    /**
     * The transaction fee in bitcoins
     *
     * @var string
     */
    private $fee;

    /**
     * The hash of the block on the local best block chain which includes this transaction,
     * encoded as hex in RPC byte order. Only returned for confirmed transactions
     *
     * @var string|null
     */
    private $blockhash;

    /**
     * The timestamp of the block that contains the transaction
     *
     * @var int|null
     */
    private $blocktime;

    /**
     * Whether the transaction is valid
     *
     * @var bool|null
     */
    private $valid;

    /**
     * If a transaction is invalid, the reason
     *
     * @var string|null
     */
    private $invalidreason;

    /**
     * The transaction version
     *
     * @var int
     */
    private $version;

    /**
     * The transaction type as number
     *
     * @var int
     */
    private $type_int;

    /**
     * The transaction type as string
     *
     * @var string
     */
    private $type;

    /**
     * @var int|null
     */
    private $propertyid;

    /**
     * @var string|null
     */
    private $amount;

    /**
     * @return string
     */
    public function getTxid(): string
    {
        return $this->txid;
    }

    /**
     * @param string $txid
     *
     * @return $this
     */
    public function setTxid(string $txid): self
    {
        $this->txid = $txid;

        return $this;
    }

    /**
     * @return string
     */
    public function getSendingaddress(): string
    {
        return $this->sendingaddress;
    }

    /**
     * @param string $sendingaddress
     *
     * @return $this
     */
    public function setSendingaddress(string $sendingaddress): self
    {
        $this->sendingaddress = $sendingaddress;

        return $this;
    }

    /**
     * @return string
     */
    public function getReferenceaddress(): string
    {
        return $this->referenceaddress;
    }

    /**
     * @param string $referenceaddress
     *
     * @return $this
     */
    public function setReferenceaddress(string $referenceaddress): self
    {
        $this->referenceaddress = $referenceaddress;

        return $this;
    }

    /**
     * @return bool
     */
    public function isMine(): bool
    {
        return $this->mine;
    }

    /**
     * @param bool $mine
     *
     * @return $this
     */
    public function setMine(bool $mine): self
    {
        $this->mine = $mine;

        return $this;
    }

    /**
     * @return int
     */
    public function getConfirmations(): int
    {
        return $this->confirmations;
    }

    /**
     * @param int $confirmations
     *
     * @return $this
     */
    public function setConfirmations(int $confirmations): self
    {
        $this->confirmations = $confirmations;

        return $this;
    }

    /**
     * @return string
     */
    public function getFee(): string
    {
        return $this->fee;
    }

    /**
     * @param string $fee
     *
     * @return $this
     */
    public function setFee(string $fee): self
    {
        $this->fee = $fee;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getBlockhash(): ?string
    {
        return $this->blockhash;
    }

    /**
     * @param null|string $blockhash
     *
     * @return $this
     */
    public function setBlockhash(?string $blockhash): self
    {
        $this->blockhash = $blockhash;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getBlocktime(): ?int
    {
        return $this->blocktime;
    }

    /**
     * @param int|null $blocktime
     *
     * @return $this
     */
    public function setBlocktime(?int $blocktime): self
    {
        $this->blocktime = $blocktime;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function isValid(): ?bool
    {
        return $this->valid;
    }

    /**
     * @param bool|null $valid
     *
     * @return $this
     */
    public function setValid(?bool $valid): self
    {
        $this->valid = $valid;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getInvalidreason(): ?string
    {
        return $this->invalidreason;
    }

    /**
     * @param null|string $invalidreason
     *
     * @return $this
     */
    public function setInvalidreason(?string $invalidreason): self
    {
        $this->invalidreason = $invalidreason;

        return $this;
    }

    /**
     * @return int
     */
    public function getVersion(): int
    {
        return $this->version;
    }

    /**
     * @param int $version
     *
     * @return $this
     */
    public function setVersion(int $version): self
    {
        $this->version = $version;

        return $this;
    }

    /**
     * @return int
     */
    public function getTypeInt(): int
    {
        return $this->type_int;
    }

    /**
     * @param int $typeInt
     *
     * @return $this
     */
    public function setTypeInt(int $typeInt): self
    {
        $this->type_int = $typeInt;

        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return $this
     */
    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getPropertyid(): ?int
    {
        return $this->propertyid;
    }

    /**
     * @param int|null $propertyid
     *
     * @return $this
     */
    public function setPropertyid(?int $propertyid): self
    {
        $this->propertyid = $propertyid;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getAmount(): ?string
    {
        return $this->amount;
    }

    /**
     * @param null|string $amount
     *
     * @return $this
     */
    public function setAmount(?string $amount): self
    {
        $this->amount = $amount;

        return $this;
    }
}
