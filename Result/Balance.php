<?php
/**
 * Balance.php
 *
 * @author tsai phan <phanalpha@hotmail.com>
 */

namespace AzureSpring\Omnius\Result;

/**
 * The token balance for a given address and property.
 */
class Balance
{
    /**
     * The available balance of the address
     *
     * @var string
     */
    private $balance;

    /**
     * The amount reserved by sell offers and accepts
     *
     * @var string
     */
    private $reserved;

    /**
     * @return string
     */
    public function getBalance(): string
    {
        return $this->balance;
    }

    /**
     * @param string $balance
     *
     * @return $this
     */
    public function setBalance(string $balance): self
    {
        $this->balance = $balance;

        return $this;
    }

    /**
     * @return string
     */
    public function getReserved(): string
    {
        return $this->reserved;
    }

    /**
     * @param string $reserved
     *
     * @return $this
     */
    public function setReserved(string $reserved): self
    {
        $this->reserved = $reserved;

        return $this;
    }
}
