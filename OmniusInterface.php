<?php
/**
 * OmniusInterface.php
 *
 * @author tsai phan <phanalpha@hotmail.com>
 */
namespace AzureSpring\Omnius;

use AzureSpring\Redbit\RedbitInterface;
use AzureSpring\Redbit\Result\FundRawTransactionResult;

/**
 * OmniCore minus interface.
 */
interface OmniusInterface extends RedbitInterface
{
    /**
     * List wallet (perhaps invalid) transactions, optionally filtered by an address and block boundaries.
     *
     * https://github.com/OmniLayer/omnicore/blob/master/src/omnicore/doc/rpc-api.md#omni_listtransactions
     *
     * @param string $address    address filter (default: "*")
     * @param int    $count      show at most n transactions (default: 10)
     * @param int    $skip       skip the first n transactions (default: 0)
     * @param int    $startBlock first block to begin the search (default: 0)
     * @param int    $endBlock   last block to include in the search (default: 999999)
     *
     * @return Result\Transaction[]
     */
    public function omniListTransactions(string $address = '*', int $count = 10, int $skip = 0, int $startBlock = 0, int $endBlock = 999999): array;

    /**
     * Get information about an Omni transaction.
     *
     * https://github.com/OmniLayer/omnicore/blob/master/src/omnicore/doc/rpc-api.md#omni_gettransaction
     *
     * @param string $txid
     *
     * @return Result\Transaction
     */
    public function omniGetTransaction(string $txid): Result\Transaction;

    /**
     * Returns the token balance for a given address and property.
     *
     * https://github.com/OmniLayer/omnicore/blob/master/src/omnicore/doc/rpc-api.md#omni_getbalance
     *
     * @param string $address    the address
     * @param int    $propertyId the property identifier
     *
     * @return Result\Balance
     */
    public function omniGetBalance(string $address, int $propertyId): Result\Balance;

    /**
     * Create the payload for a simple send transaction.
     *
     * https://github.com/OmniLayer/omnicore/blob/master/src/omnicore/doc/rpc-api.md#omni_createpayload_simplesend
     *
     * @param int    $propertyId the identifier of the tokens to send
     * @param string $amount     the amount to send
     *
     * @return string
     */
    public function omniCreateSimpleSend(int $propertyId, string $amount): string;

    /**
     * Adds a payload with class C (op-return) encoding to the transaction.
     *
     * https://github.com/OmniLayer/omnicore/blob/master/src/omnicore/doc/rpc-api.md#omni_createrawtx_opreturn
     *
     * @param string $rawTx   the raw transaction to extend (can be null)
     * @param string $payload the hex-encoded payload to add
     *
     * @return string
     */
    public function omniCreateRawTransaction(?string $rawTx, string $payload): string;

    /**
     * Adds a reference output to the transaction.
     *
     * https://github.com/OmniLayer/omnicore/blob/master/src/omnicore/doc/rpc-api.md#omni_createrawtx_reference
     *
     * @param string|null $rawTx      the raw transaction to extend (can be null)
     * @param string      $refAddress the reference address or destination
     * @param string|null $amount     the optional reference amount (minimal by default)
     *
     * @return string
     */
    public function omniCreateRawTransactionReference(?string $rawTx, string $refAddress, ?string $amount = null): string;

    /**
     * Create a (unsigned) simple send transaction.
     *
     * @param string      $fromAddress
     * @param string      $toAddress
     * @param int         $propertyId
     * @param string      $amount
     * @param string|null $changeAddress
     *
     * @return FundRawTransactionResult
     */
    public function omniCreateRawSimpleSendTransaction(string $fromAddress, string $toAddress, int $propertyId, string $amount, ?string $changeAddress = null): FundRawTransactionResult;
}
